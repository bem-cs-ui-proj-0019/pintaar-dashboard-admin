<?php

namespace App\Override;

use Illuminate\Support\Facades\Hash;

class AdministratorData
{
    public $id = 1;
    public $name = 'Administrator';
    public $email = 'admin@pintaar.com';
    public $email_verified_at = null;
    public $password = '123456';            // for authentication
    public $remember_token = null;
    public $created_at = null;
    public $updated_at = null;
}
