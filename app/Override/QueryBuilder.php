<?php

namespace App\Override;

use App\Models\Administrator;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class QueryBuilder extends \Illuminate\Database\Query\Builder {

    /**
    * The table which the query is targeting.
    *
    * @var string
    */
    public $from;

    /**
     * Set the table which the query is targeting.
     *
     * @param  string  $table
     * @return $this
     */
    public function from($table)
    {
        $this->from = $table;

        return $this;
    }

    //@Override
    public function get($columns = ['*']) {
        if ($this->from == 'administrators') {
            // customize the call
            $adminData = new AdministratorData;
            $adminData->password = bcrypt($adminData->password);

            $administrator = collect([$adminData]);

            return $administrator;

        } else {
            // return default
            return parent::get($columns);
        }
    }
}
